local core = require('openmw.core')
local self = require('openmw.self')
local nearby = require('openmw.nearby')
local util = require('openmw.util')
local I = require('openmw.interfaces')

local common = require('scripts.RandomNPC.common')

I.Settings.registerPage {
    key = 'urm_RandomNPC',
    l10n = 'urm_RandomNPC',
    name = 'page_name',
    description = 'page_description',
}

local NavigateWithoutSwimming = util.bitOr(nearby.NAVIGATOR_FLAGS.UsePathgrid, nearby.NAVIGATOR_FLAGS.Walk)

local function findNavPosition(position)
    local _, steps = nearby.findPath(position, position, {
        includeFlags = NavigateWithoutSwimming,
    })
    return steps and steps[1]
end

local HeadOffset = util.vector3(0, 0, 123)

local function randomPosition()
    local navPos = findNavPosition(self.position)
    local randomPos = nearby.findRandomPointAroundCircle(navPos, common.cellSize * 0.5, {
        includeFlags = NavigateWithoutSwimming,
    })
    local sightlineCheck = nearby.castRay(self.position + HeadOffset, randomPos + HeadOffset, {
        ignore = self,
    })
    if sightlineCheck.hit then
        return randomPos
    else
        return nil
    end
end

return {
    eventHandlers = {
        [common.Events.RequestPosition] = function(e)
            local position = randomPosition()
            if position then
                core.sendGlobalEvent(common.Events.Spawn, {
                    cell = e.cell,
                    position = position
                })
            end
        end,
    },
}

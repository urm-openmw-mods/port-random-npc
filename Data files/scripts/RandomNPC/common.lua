local function cellId(cell)
    if cell.isExterior then
        return ('%d %d'):format(cell.gridX, cell.gridY)
    else
        return cell.name
    end
end

local function cellNameFromid(id)
    return id:match('[-]?[%d]+ [-]?[%d]+') and '' or id
end

local Events = {
    RequestPosition = 'urm_RandomNPC_request_position',
    Spawn = 'urm_RandomNPC_spawn',
    Despawn = 'urm_RandomNPC_despawn',
}

return {
    cellId = cellId,
    cellNameFromid = cellNameFromid,
    Events = Events,
    cellSize = 8192,
}

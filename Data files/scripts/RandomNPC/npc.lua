local core = require('openmw.core')
local self = require('openmw.self')
local storage = require('openmw.storage')
local I = require('openmw.interfaces')

local common = require('scripts.RandomNPC.common')

local cellId = nil
local createdAt = nil

local settings = storage.globalSection('urm_RandomNPC_Gameplay')

local function despawnCheck()
    if not createdAt or not cellId then return end
    local lifetime = settings:get('npcLifetime') * 60 * 60
    if core.getGameTime() - createdAt > lifetime then
        core.sendGlobalEvent(common.Events.Despawn, { cell = cellId, npc = self.object })
    end
end

return {
    engineHandlers = {
        onInit = function(e)
            cellId = e.cell
            createdAt = core.getGameTime()
            I.AI.startPackage {
                type = 'Wander',
                distance = common.cellSize * 0.5,
            }
        end,
        onSave = function()
            return {
                cellId = cellId,
                createdAt = createdAt,
            }
        end,
        onLoad = function(saved)
            if not saved then return end
            cellid = saved.cellId
            createdAt = saved.createdAt
        end,
        onActive = despawnCheck,
        onInactive = despawnCheck,
    }
}
